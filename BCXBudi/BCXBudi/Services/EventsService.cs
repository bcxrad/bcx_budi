﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using BCXBudi.Adapters;
using BCXBudi.Models;
using Newtonsoft.Json;

namespace BCXBudi.Services
{
    public class EventsService : IRestFul<Task<EventList>>
    {
        HttpClient client;

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public void Patch()
        {
            throw new NotImplementedException();
        }

        public void Post()
        {
            throw new NotImplementedException();
        }

        public void Put()
        {
            throw new NotImplementedException();
        }

        public async Task<EventList> Get()
        {
            EventList eventList = new EventList();
            var uri = new Uri(string.Format(Constants.EVENT_SERVICE_REST_URL, string.Empty));
            client = new HttpClient();
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                eventList = JsonConvert.DeserializeObject<EventList>(content);
                Console.WriteLine(eventList.transactionId);
            }
            return eventList;
        }
    }
}
