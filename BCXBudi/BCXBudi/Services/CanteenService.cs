﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using BCXBudi.Adapters;
using BCXBudi.Models;
using Newtonsoft.Json;

namespace BCXBudi.Services
{
    public class CanteenService : IRestFul<Task<Canteen>>
    {
        HttpClient client;

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public void Patch()
        {
            throw new NotImplementedException();
        }

        public void Post()
        {
            throw new NotImplementedException();
        }

        public void Put()
        {
            throw new NotImplementedException();
        }

        public async Task<Canteen> Get()
        {
            Canteen canteen = new Canteen();
            var uri = new Uri(string.Format(Constants.CANTEEN_SERVICE_REST_URL + DateTime.Now.ToString("yyyy-MM-dd"), string.Empty));
            client = new HttpClient();
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                canteen = JsonConvert.DeserializeObject<Canteen>(content);
                Console.WriteLine(canteen.transactionId);
            }
            return canteen;
        }
    }
}
