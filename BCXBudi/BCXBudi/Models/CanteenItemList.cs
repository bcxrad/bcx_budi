﻿using System;
namespace BCXBudi.Models
{
    public class CanteenItemList
    {
        public string id { get; set; }
        public string name { get; set; }
        public string price { get; set; }
        public string imageUrl { get; set; }
        public string date { get; set; }
    }
}
