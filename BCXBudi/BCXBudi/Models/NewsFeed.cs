﻿using System;
using System.Collections.Generic;

namespace BCXBudi.Models
{
    public class NewsFeed
    {
        public NewsFeed()
        {
        }
        public string transactionId { get; set; }
        public List<NewsEntry> newsEntries { get; set; }
    }
}
