﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using BCXBudi.Tables;
using SQLite;

namespace BCXBudi.Storage
{
    public class EmployeeDatabase
    {
        readonly SQLiteAsyncConnection database;
        public string EmployeeNumber;

        public EmployeeDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<EmployeeTable>().Wait();
        }

        public async Task<int> CleanRecords() {
            try
            {
                await database.ExecuteAsync("DELETE FROM EmployeeTable");
            } 
            catch (Exception e) 
            {
                Debug.WriteLine(e.Message);
                return 1;
            }
            return 0;
        }

        public async Task<int> UpdateEmployeeGeneric(string value, string column) {
            Debug.WriteLine("UPDATE EmployeeTable SET " + column + " = '" + value + "' WHERE EmployeeNumber = '" + EmployeeNumber + "'");
            try
            {
                await database.ExecuteAsync("UPDATE EmployeeTable SET " + column + " = '" + value + "' WHERE EmployeeNumber = '" + EmployeeNumber + "'");
            }
            catch (Exception e) 
            {
                Debug.WriteLine(e.Message);
                return 1;
            }
            return 0;
        }

        public async Task<int> UpdateEmployeeReflection(Object value, Object field)
        {
            // To do - add reflection
            try
            {
                Task<EmployeeTable> employee = database.Table<EmployeeTable>().Where(i => i.EmployeeNumber == this.EmployeeNumber).FirstOrDefaultAsync();
                if(employee.Result != null) 
                {
                    employee.Result.RememberMe = Boolean.Parse(value.ToString());
                    return await database.UpdateAsync(employee.Result);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return 1;
            }
            return 0;
        }

        public void SetEmployeeNumberFromRememberMe() {
            Task<EmployeeTable> employee = database.Table<EmployeeTable>().Where(i => i.RememberMe == true).FirstOrDefaultAsync();
            if (employee.Result != null || employee.Result.RememberMe != false)
            {
                this.EmployeeNumber = employee.Result.EmployeeNumber;
            }
        }

        public string GetProfilePicLocation() {
            return database.Table<EmployeeTable>().Where(i => i.EmployeeNumber == EmployeeNumber).FirstOrDefaultAsync().Result.ProfilePicLocation;
        }

        public bool GetRememberMe() {
            Task<EmployeeTable> employee = database.Table<EmployeeTable>().Where(i => i.RememberMe == true).FirstOrDefaultAsync();
            if (employee.Result == null || employee.Result.RememberMe == false) {
                return false;
            }
            return true;
        }

        public Task<EmployeeTable> GetUsername()
        {
            return database.Table<EmployeeTable>().Where(i => i.EmployeeNumber == EmployeeNumber).FirstOrDefaultAsync();
        }

        public Task<EmployeeTable> GetProfileOnRememberMe()
        {
            return database.Table<EmployeeTable>().Where(i => i.RememberMe == true).FirstOrDefaultAsync();
        }


        public Task<EmployeeTable> GetProfile() 
        {
            return database.Table<EmployeeTable>().Where(i => i.EmployeeNumber == EmployeeNumber).FirstOrDefaultAsync();
        }

        public Task<int> InsertEmployeeInfoOnLogin(EmployeeTable employeeTable) {
            return database.InsertAsync(employeeTable);
        }
    }
}
