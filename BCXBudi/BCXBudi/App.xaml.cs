using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BCXBudi.Storage;
using System.IO;
using BCXBudi.Pages;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using System.Threading.Tasks;
using BCXBudi.Tables;
using System.Diagnostics;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace BCXBudi
{
    public partial class App : Application
    {
        static EmployeeDatabase database;
        static Files files;

        public App()
        {
            InitializeComponent();
            if(Database.GetRememberMe()) 
            {
                Debug.WriteLine("Remember me true, going to HomePage");
                Database.SetEmployeeNumberFromRememberMe();
                MainPage = new MainPage();
            }
            else 
            {
                Debug.WriteLine("Remember me false, going to LoginPage");
                MainPage = new Login();
            }
        }

        public static Files Files  
        {
            get
            {
                return files;
            }
        }

        public static EmployeeDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new EmployeeDatabase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "EmployeeDBSQLite.db3"));
                }
                return database;
            }
        }

        protected override void OnStart()
        {
            AppCenter.Start("android=779089fb-dc9c-45d6-854f-9f649915322c;" +
                  "uwp={Your UWP App secret here};" +
                  "ios=b9ed637a-8c65-44ca-ac64-1a1f2aef2a09",
                  typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
