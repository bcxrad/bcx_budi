﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace BCXBudi.Pages
{
    public partial class News : ContentPage
    {
        public string heading { get; set; }
        public string body { get; set; }
        public string imageUrl { get; set; }

        public News(string heading, string body, string imageUrl)
        {
            this.heading = heading;
            this.body = body;
            this.imageUrl = imageUrl;
            InitializeComponent();
            BindingContext = this;
        }
    }
}
