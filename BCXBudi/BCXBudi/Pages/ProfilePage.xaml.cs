﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using BCXBudi.Storage;
using BCXBudi.Tables;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

namespace BCXBudi.Pages
{
    
    public partial class ProfilePage : ContentPage
    {
        private MediaFile mediaFile;
        public string Name { get; set; }
        public string Designation { get; set; }
        public Boolean EditMode { get; set; }
        public string ProfilePicture { get; set; }
        private int tapCount;

        public ProfilePage()
        {
            InitializeComponent();
            InflateProfile();
            BindingContext = this;
        }
        private async void EditProfile_Clicked(object sender, EventArgs e)
        {
            DesignationName.IsEnabled = true;
            DesignationName.Text = string.Empty;
            DesignationName.Placeholder = Designation;
            ProfileName.IsEnabled = true;
            ProfileName.Text = string.Empty;
            ProfileName.Placeholder = Name;
            SaveProfile.IsVisible = true;
        }

        private async void SaveProfile_Clicked(object sender, EventArgs e)
        {
            int errorCode = 0;
            DesignationName.IsEnabled = false;
            ProfileName.IsEnabled = false;
            SaveProfile.IsVisible = false;

            if(DesignationName.Text != string.Empty) {
                errorCode += await App.Database.UpdateEmployeeGeneric(DesignationName.Text, "Designation");
            }
            if(ProfileName.Text != string.Empty) {
                errorCode += await App.Database.UpdateEmployeeGeneric(ProfileName.Text, "Name");
            }
            Task<int> UIReady = InflateProfile();
        }

        private async void SignOut_Clicked(object sender, EventArgs e)
        {
            var answer = await DisplayAlert("Sign Out?", "Are you sure you want to Sign Out?", "Yes", "No");
            if(answer) {
                await App.Database.UpdateEmployeeReflection(false, null);
                Application.Current.MainPage = new Login();
            }
        }

        private async void SurveyButton_Clicked(object sender, EventArgs e)
        {
            var answer = await DisplayAlert("Sign Out?", "Are you sure you want to Sign Out?", "Yes", "No");
            if (answer)
            {
                await App.Database.UpdateEmployeeReflection(false, null);
                Application.Current.MainPage = new Login();
            }

        }

        public async Task<int> OnTapGestureRecognizerTapped(object sender, EventArgs args)
        {
            int errorCode = 0;
            tapCount++;
            if (tapCount == 1)
            {
                try
                {
                    var imageSender = (Image)sender;
                    await CrossMedia.Current.Initialize();
                    if (!CrossMedia.Current.IsPickPhotoSupported)
                    {
                        await DisplayAlert("Pick Photo not Supported", "Please enable permission to pick photo", "OK");
                        return 1;
                    }
                    mediaFile = await CrossMedia.Current.PickPhotoAsync();

                    if (mediaFile == null)
                    {
                        return 0;
                    }
                    errorCode += await App.Database.UpdateEmployeeGeneric(mediaFile.Path, "ProfilePicLocation");
                    profilePic.Source = ImageSource.FromStream(() =>
                    {
                        return mediaFile.GetStream();
                    });
                } catch (Exception e) {
                } finally {
                    tapCount = 0;
                }
            }
            return errorCode;
        }

        private async Task<int> CameraButton_Clicked(object sender, EventArgs e)
        {
            int errorCode = 0;
            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                if (status != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Camera))
                    {
                        await DisplayAlert("Camera Permission", "Allow BCXBudi to access your camera", "OK");
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera });
                    status = results[Permission.Camera];
                }

                if (status == PermissionStatus.Granted)
                {
                    await CrossMedia.Current.Initialize();

                    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                    {
                        await DisplayAlert("Camera not Supported", "Please enable permission to take photo", "OK");
                        return 1;
                    }

                    mediaFile = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                    {
                        Name = "Profile.jpg"
                    });

                    if (mediaFile == null)
                    {
                        return 0;
                    }
                    errorCode += await App.Database.UpdateEmployeeGeneric(mediaFile.Path, "ProfilePicLocation");
                    profilePic.Source = ImageSource.FromStream(() =>
                    {
                        return mediaFile.GetStream();
                    });

                }
                else if (status != PermissionStatus.Unknown)
                {
                    await DisplayAlert("Camera Denied", "Can not continue, try again.", "OK");
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message.ToString(), "OK");
            }
            return errorCode;
        }

        private async Task<int> InflateProfile() {
            Task<EmployeeTable> employeeTable = App.Database.GetProfile();
            try
            {
                if (App.Database.GetProfilePicLocation() != null)
                {
                    Debug.WriteLine(App.Database.GetProfilePicLocation());
                    profilePic.Source = ImageSource.FromFile(App.Database.GetProfilePicLocation());
                }
                else
                {
                    profilePic.Source = ImageSource.FromResource("BCXBudi.Images.blank_profile.png");
                }
            }
            catch (Exception e) {
                Debug.WriteLine(e.Message);
            }
            if (employeeTable.Result == null || employeeTable.Result.Name == null)
            {
                Name = "Name & Surname";
            }
            else 
            {
                Name = employeeTable.Result.Name + " " + employeeTable.Result.Surname;
            }
            if (employeeTable.Result == null || employeeTable.Result.Designation == null)
            {
                Designation = "Occupation";
            }
            else
            {
                Designation = employeeTable.Result.Designation;
            }
            return 1;
        }
    }
}
