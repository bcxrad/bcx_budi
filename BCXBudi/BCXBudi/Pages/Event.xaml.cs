﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace BCXBudi.Pages
{
    public partial class Event : ContentPage
    {
        public string title { get; set; }
        public string description { get; set; }
        public string imageUrl { get; set; }
        public string address { get; set; }
        public string date { get; set; }

        public Event(string title, string description, string imageUrl, string address, string date)
        {
            this.title = title;
            this.description = description;
            this.imageUrl = imageUrl;
            this.address = address;
            this.date = date;
            InitializeComponent();
            BindingContext = this;
        }
    }
}
