﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using BCXBudi.Adapters;
using BCXBudi.Models;
using BCXBudi.Services;
using Xamarin.Forms;
using XamForms.Controls;

namespace BCXBudi.Pages
{
    public partial class EventsPage : ContentPage
    {
        public EventsPage()
        {
            InitializeComponent();
            BindingContext = this;

            eventsCalendar.SpecialDates = new List<SpecialDate>{
                new SpecialDate(DateTime.Now.AddDays(2)) { BackgroundColor = Color.Gray, TextColor = Color.Red, Selectable = true }
            };
        }

        void Handle_DateClicked(object sender, XamForms.Controls.DateTimeEventArgs e)
        {
            //selectedDate.Text = e.DateTime.ToString();
        }
    }
}
