﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using BCXBudi.Tables;
using Xamarin.Forms;

namespace BCXBudi.Pages
{
    public partial class Login : ContentPage
    {
        public Login()
        {
            InitializeComponent();
            LoginFormEnabled(false);
            submit.IsEnabled = false;
        }

        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            
            if(employeePicker.SelectedIndex == 0) {
                LoginFormEnabled(false);
            } else {
                LoginFormEnabled(true);
            }
            submit.IsEnabled = true;
        }

        private async void ReadMoreBtnClick(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new ReadMorePage());
        }
        private async Task<int> SubmitBtnClick(object sender, EventArgs e)
        {
            int errorCode = 0;
            if(!CheckCredentialValidity()) {
                messageLabel.Text = "Incorrect Credentials, please check Email and Password";
                passwordEntry.Text = string.Empty;
            }
            else
            {
                if (employeePicker.SelectedIndex == 1)
                {
                    App.Database.EmployeeNumber = usernameEntry.Text;
                    EmployeeTable employeeTable = App.Database.GetProfile().Result;
                    if(employeeTable == null) {
                        employeeTable = new EmployeeTable();
                        employeeTable.RememberMe = rememberMe.IsToggled;
                        employeeTable.EmployeeNumber = usernameEntry.Text;
                        errorCode += await App.Database.CleanRecords();
                        errorCode += await App.Database.InsertEmployeeInfoOnLogin(employeeTable);
                    } 
                    else 
                    {
                        errorCode += await App.Database.UpdateEmployeeReflection((Object) rememberMe.IsToggled, null);
                    }
                    Application.Current.MainPage = new MainPage();
                }
                else if (employeePicker.SelectedIndex == 0)
                {
                    Application.Current.MainPage = new NewEmployeeMainPage();
                }
            }
            return errorCode;
        }

        private void LoginFormEnabled(bool selection) {
            usernameEntry.IsEnabled = selection;
            passwordEntry.IsEnabled = selection;
        }

        private bool CheckCredentialValidity() {
            if (usernameEntry.Text == "" || passwordEntry.Text == "")
            {
                return false;
            }
            return true;
        }
    }
}
