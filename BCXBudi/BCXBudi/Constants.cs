﻿using System;
namespace BCXBudi
{
    public class Constants
    {
        public static string NEWS_SERVICE_REST_URL = "https://bcxnewsfeedapi.azurewebsites.net/api/newsfeed";
        public static string CANTEEN_SERVICE_REST_URL = "https://bcxcanteenapi.azurewebsites.net/api/canteen/";
        public static string EVENT_SERVICE_REST_URL = "https://bcxeventsapi.azurewebsites.net/api/events/";
        public static string BLOB_STORAGE_REST_URL = "https://highvelocitystorage.blob.core.windows.net/employee-app/";

        public static string CLIENT_ID = "ec08caca-ab7b-4847-9a5e-9d0a1539491a";
        public static string AUTHORITY = "https://login.windows.net/common";
        public static string RETURN_URI = "http://infinity-app-redirect";
        public const string GRAPH_RESOURCE_URI = "https://graph.windows.net";
    }
}
